package com.example.niepodam.test123;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;

public class FullscreenActivity extends AppCompatActivity {

    private View mContentView;
    private View mControlsView;
    private int[] RGB_vars; //there's no typo!




    private final SeekBar.OnSeekBarChangeListener mSeekBarChangeListener = new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int progressValue, boolean fromUser){
            RGB_vars[0] = progressValue;
            //Get color from three RGB values
            int ARGB_ = 0xFF000000+RGB_vars[0]*16*16*16*16+RGB_vars[1]*16*16+RGB_vars[2];

            //Set rectangle's color
            findViewById(R.id.rect).setBackgroundColor(ARGB_);
            //DEBUG
            ((TextView) findViewById(R.id.textView4)).setText(Integer.toHexString(ARGB_));
        }
        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {}

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {}
    };

    //Copy of mSeekBarChangeListener (with only 1 line of code changed)
    private final SeekBar.OnSeekBarChangeListener mSeekBarChangeListener2 = new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int progressValue, boolean fromUser){
            RGB_vars[1] = progressValue;
            //Get color from three RGB values
            int ARGB_ = 0xFF000000+RGB_vars[0]*16*16*16*16+RGB_vars[1]*16*16+RGB_vars[2];

            //Set rectangle's color
            findViewById(R.id.rect).setBackgroundColor(ARGB_);
            //DEBUG
            ((TextView) findViewById(R.id.textView4)).setText(Integer.toHexString(ARGB_));
        }
        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {}

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {}
    };

    //Copy of mSeekBarChangeListener (with only 1 line of code changed)
    private final SeekBar.OnSeekBarChangeListener mSeekBarChangeListener3 = new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int progressValue, boolean fromUser){
            RGB_vars[2] = progressValue;
            //Get color from three RGB values
            int ARGB_ = 0xFF000000+RGB_vars[0]*16*16*16*16+RGB_vars[1]*16*16+RGB_vars[2];

            //Set rectangle's color
            findViewById(R.id.rect).setBackgroundColor(ARGB_);
            //DEBUG
            ((TextView) findViewById(R.id.textView4)).setText(Integer.toHexString(ARGB_));
        }
        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {}

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {}
    };



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_fullscreen);
        findViewById(R.id.rect).setBackgroundColor(0xFF000000);

        mControlsView = findViewById(R.id.fullscreen_content_controls);
        mContentView = findViewById(R.id.fullscreen_content);

        //Set event listeners
        ((SeekBar) findViewById(R.id.Red_bar)).setOnSeekBarChangeListener(mSeekBarChangeListener);
        ((SeekBar) findViewById(R.id.Green_bar)).setOnSeekBarChangeListener(mSeekBarChangeListener2);
        ((SeekBar) findViewById(R.id.Blue_bar)).setOnSeekBarChangeListener(mSeekBarChangeListener3);

        //Initialize RGB_vats
        RGB_vars = new int[3];

        RGB_vars[0] = 0;
        RGB_vars[1] = 0;
        RGB_vars[2] = 0;
    }
}
